import { appConfig } from "../config/app.js";
import {BaseHttp} from "./base_http";

export const http = new BaseHttp(appConfig.apiUrl);
