import superagent from "superagent";
import superagentIntercept from "superagent-intercept";
import { appConfig } from "../config/app.js";
import { TokenUtil } from "./token";
import { attachSuperagentLogger } from "./http_logger";

let AuthIntercept = superagentIntercept((err, res) => {
  if (res && res.status === 401) {
    TokenUtil.clearAccessToken();
    // TokenUtil.persistToken();
    // window.location.href = "/admin/login";
  }
});

const localStorage = typeof window === "undefined" ? { getItem: () => {} } : window.localStorage;

export class BaseHttp {
  baseUrl;

  constructor(baseUrl) {
    this.baseUrl = baseUrl;
  }

  fetcher = async (url) => {
    let req = superagent
      .get(this.baseUrl + url)
      .use(AuthIntercept)
      .use(attachSuperagentLogger);

    req = req.set("Authorization", "Bearer " + localStorage.getItem("token"));

    const resp = await req;

    // if(resp.body.results) {
    //     return resp.body.results;
    // }
    return resp.body;
  };
  get = (url, opts = {}) => {
    let req = superagent
      .get(this.baseUrl + url)
      .use(AuthIntercept)
      .use(attachSuperagentLogger);

    req = req.set("Authorization", "Bearer " + localStorage.getItem("token"));

    return req;
  };
  post = (url, opts) => {
    let req = superagent
      .post(this.baseUrl + url)
      .use(AuthIntercept)
      .use(attachSuperagentLogger);

    req = req.set("Authorization", "Bearer " + localStorage.getItem("token"));
    return req;
  };
  put = (url, opts) => {
    let req = superagent
      .put(this.baseUrl + url)
      .use(AuthIntercept)
      .use(attachSuperagentLogger);

    req = req.set("Authorization", "Bearer " + localStorage.getItem("token"));
    return req;
  };
  del = (url, opts) => {
    let req = superagent
      .del(this.baseUrl + url)
      .use(AuthIntercept)
      .use(attachSuperagentLogger);
    req = req.set("Authorization", "Bearer " + localStorage.getItem("token"));
    return req;
  };
  upload = (url) => {
    let req = superagent.post(this.baseUrl + url).use(AuthIntercept);
    req = req.set("Authorization", "Bearer " + localStorage.getItem("token"));

    return req;
  };

  uploadAntd = (args) => {
    const file = args.file;
    const request = http.upload(file).use(AuthIntercept);

    request
      .on("progress", (event) => {
        args.onProgress(event);
      })
      .then((it) => {
        args.onSuccess(it);
      })
      .catch((err) => {
        args.onError(err);
      });

    return request;
  };
}
