export class TokenUtil {
  static accessToken = null;
  static refreshToken = null;

  static loadToken() {
    if (typeof window === "undefined") {
      return;
    }

    const accessToken = localStorage.getItem("access_token");
    // const refreshToken = localStorage.getItem("refresh_token");

    if (accessToken) {
      TokenUtil.setAccessToken(accessToken);
    }

    // if (refreshToken) {
    //   TokenUtil.setRefreshToken(refreshToken);
    // }

    if (TokenUtil.accessToken != null) {
      localStorage.setItem("access_token", TokenUtil.accessToken);
    }
  }

  static persistToken() {
    if(TokenUtil.accessToken != null) {
      localStorage.setItem('access_token', TokenUtil.accessToken);
    } else {
      localStorage.removeItem('access_token');
    }
  }

  static setAccessToken(accessToken) {
    TokenUtil.accessToken = accessToken;
  }

  // static setRefreshToken(refreshToken) {
  //   TokenUtil.refreshToken = refreshToken;
  // }

  static clearAccessToken() {
    TokenUtil.accessToken = null;
  }
  // static clearRefreshToken() {
  //   TokenUtil.accessToken = null;
  // }
}

// export class TokenUtil {
//   static accessToken = null;
//   static refreshToken = null;
//
//   static loadToken() {
//     if (typeof window === "undefined") {
//       return;
//     }
//
//     const accessToken = localStorage.getItem("token");
//
//     if (accessToken) {
//       TokenUtil.setAccessToken(accessToken);
//     }
//
//     if (refreshToken) {
//       TokenUtil.setRefreshToken(refreshToken);
//     }
//   }
//
//   static setAccessToken(accessToken) {
//     TokenUtil.accessToken = accessToken;
//   }
//
//   static setRefreshToken(refreshToken) {
//     TokenUtil.refreshToken = refreshToken;
//   }
//
//   static clearAccessToken() {
//     TokenUtil.accessToken = null;
//   }
//   static clearRefreshToken() {
//     TokenUtil.accessToken = null;
//   }
// }
