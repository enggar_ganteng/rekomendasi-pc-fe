import { makeAutoObservable, runInAction } from "mobx";
import { TokenUtil } from "../utils/token";
import AuthRepository from "../repository/auth";

export class SearchStore {
    dataSearch = {
        processor: null,
        motherboard: null,
        memory:null,
        casing:null,
        ssd:null,
        vga:null,
        mouse:null,
        keyboard:null,
        monitor:null,
    };
    ctx;

    constructor(ctx) {
        makeAutoObservable(this);
        this.ctx = ctx;
    }

    async searchRecomendation(data) {
        this.dataSearch = data;
    }
}
