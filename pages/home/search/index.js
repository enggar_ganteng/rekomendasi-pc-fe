import React from "react";
import DefaultLayout from "../../../components/Layout/DefaultLayout";
import {Button, Card, PageHeader} from "antd";
import {observer} from "mobx-react-lite";
import {useStore} from "../../../components/StoreProvider";
import RecommendationRepository from "../../../repository/recommendation";

const Search = observer(() => {
    const store = useStore();

    const {data} = RecommendationRepository.hooks.getPC();

    console.log('get Data coy', data)

    return <div>
        <PageHeader
            style={{
            }}
            title={"Search"}
        >
        </PageHeader>
        <Card title={"Recomendation PC"} className={"flex text-gray-500"}>

        </Card>
        {store.search.dataSearch.mouse && (
            <Card title={"Recomendation Mouse"}>

            </Card>
        )}

        {store.search.dataSearch.keyboard && (
            <Card title={"Recomendation Keyboard"}>

            </Card>
        )}

        {store.search.dataSearch.monitor && (
            <Card title={"Recomendation Monitor"}>

            </Card>
        )}
    </div>
});

Search.getLayout = function Layout(page) {
    return <DefaultLayout>
        {page}
    </DefaultLayout>;
};

export default Search;
