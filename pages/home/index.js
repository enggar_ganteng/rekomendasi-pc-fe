import React,{ useEffect, useState } from "react";
import DefaultLayout from "../../components/Layout/DefaultLayout";
import {Button, Card, Avatar, Breadcrumb, Col, Form, Input, PageHeader, Row, Select} from "antd";
import {observer} from "mobx-react-lite";
import {useStore} from "../../components/StoreProvider";
import {useRouter} from "next/router"

import DataProccessor from "../../data/DataProccessor";
import DataMotherboard from "../../data/DataMotherboard";
import DataMemory from "../../data/DataMemory";
import DataCasing from "../../data/DataCasing";
import DataSsd from "../../data/DataSsd";
import DataVga from "../../data/DataVga";
import DataMouse from "../../data/DataMouse";
import DataKeyboard from "../../data/DataKeyboard";
import DataMonitor from "../../data/DataMonitor";

const {Option} = Select;

const Dashboard = observer(() => {
    const [form] = Form.useForm();
    const store = useStore();
    const router = useRouter();

    const [processor, setProcessor] = useState({name: '', price: 0});
    const [motherBoard, setMotherBoard] = useState({name: '', price: 0});
    const [memory, setMemory] = useState({name: '', price: 0});
    const [casing, setCasing] = useState({name: '', price: 0});
    const [ssd, setSsd] = useState({name: '', price: 0});
    const [vga, setVga] = useState({name: '', price: 0});
    const [mouse, setMouse] = useState({name: '', price: 0});
    const [keyboard, setKeyboard] = useState({name: '', price: 0});
    const [monitor, setMonitor] = useState({name: '', price: 0});
    const [totalPrice, setTotalPrice] = useState(0);

    useEffect(() => {
        setTotalPrice(processor.price + motherBoard.price + memory.price + casing.price + ssd.price + vga.price + mouse.price + keyboard.price + monitor.price)
    }, [processor,motherBoard,memory,casing,ssd,vga,mouse,keyboard,monitor, totalPrice]);

    return <div>
        <PageHeader
            style={{
            }}
            title={"Home"}
        >
        </PageHeader>
        <div className={"flex flex-row"} style={{width:'100%',}}>
            <div style={{width:"70%"}}>
                <Form
                    form={form}
                    layout="vertical"
                >
                    <Card title={"Core Component"}>
                        <Form.Item
                            name="processor"
                            label="Processor"
                            style={{borderBottom: 25}}
                            rules={[{required: true}]}
                        >
                            <Select
                                style={{width: "100%"}}
                                placeholder="Pilih Processor"
                                onChange={(e)=> {
                                    const filter = DataProccessor.filter(it => it.name === e);
                                    setProcessor({
                                        name : e,
                                        price: filter[0].price
                                    });
                                }}
                            >
                                {DataProccessor?.map((d,idx) => {
                                    return (
                                        <Option key={idx} value={d.name}>
                                            {d.name}
                                        </Option>
                                    );
                                })}
                            </Select>
                        </Form.Item>

                        <Form.Item
                            name="motherboard"
                            label="Motherboard"
                            style={{borderBottom: 25}}
                            rules={[{required: true}]}
                        >
                            <Select
                                style={{width: "100%"}}
                                placeholder="Pilih Motherboard"
                                onChange={(e)=> {
                                    const filter = DataMotherboard.filter(it => it.name === e);
                                    setMotherBoard({
                                        name : e,
                                        price: filter[0].price
                                    });
                                }}
                            >
                                {DataMotherboard?.map((d,idx) => {
                                    return (
                                        <Option key={idx} value={d.name}>
                                            {d.name}
                                        </Option>
                                    );
                                })}
                            </Select>
                        </Form.Item>

                        <Form.Item
                            name="memory"
                            label="Ram"
                            style={{borderBottom: 25}}
                            rules={[{required: true}]}
                        >
                            <Select
                                style={{width: "100%"}}
                                placeholder="Pilih Ram"
                                onChange={(e)=> {
                                    const filter = DataMemory.filter(it => it.name === e);
                                    setMemory({
                                        name : e,
                                        price: filter[0].price
                                    });
                                }}
                            >
                                {DataMemory?.map((d,idx) => {
                                    return (
                                        <Option key={idx} value={d.name}>
                                            {d.name}
                                        </Option>
                                    );
                                })}
                            </Select>
                        </Form.Item>
                    </Card>

                    <Card title={"Main Component"} style={{marginTop:30, marginBottom:50}}>
                        <Form.Item
                            name="casing"
                            label="Casing"
                            style={{borderBottom: 25}}
                            rules={[{required: true}]}
                        >
                            <Select
                                style={{width: "100%"}}
                                placeholder="Pilih Casing"
                                onChange={(e)=> {
                                    const filter = DataCasing.filter(it => it.name === e);
                                    setCasing({
                                        name : e,
                                        price: filter[0].price
                                    });
                                }}
                            >
                                {DataCasing?.map((d,idx) => {
                                    return (
                                        <Option key={idx} value={d.name}>
                                            {d.name}
                                        </Option>
                                    );
                                })}
                            </Select>
                        </Form.Item>

                        <Form.Item
                            name="ssd"
                            label="SSD"
                            style={{borderBottom: 25}}
                            rules={[{required: true}]}
                        >
                            <Select
                                style={{width: "100%"}}
                                placeholder="Pilih SSD"
                                onChange={(e)=> {
                                    const filter = DataSsd.filter(it => it.name === e);
                                    setSsd({
                                        name : e,
                                        price: filter[0].price
                                    });
                                }}
                            >
                                {DataSsd?.map((d,idx) => {
                                    return (
                                        <Option key={idx} value={d.name}>
                                            {d.name}
                                        </Option>
                                    );
                                })}
                            </Select>
                        </Form.Item>

                        <Form.Item
                            name="vga"
                            label="VGA"
                            style={{borderBottom: 25}}
                            rules={[{required: true}]}
                        >
                            <Select
                                style={{width: "100%"}}
                                placeholder="Pilih VGA"
                                onChange={(e)=> {
                                    const filter = DataVga.filter(it => it.name === e);
                                    setVga({
                                        name : e,
                                        price: filter[0].price
                                    });
                                }}
                            >
                                {DataVga?.map((d,idx) => {
                                    return (
                                        <Option key={idx} value={d.name}>
                                            {d.name}
                                        </Option>
                                    );
                                })}
                            </Select>
                        </Form.Item>

                        <Form.Item
                            name="mouse"
                            label="Mouse"
                            style={{borderBottom: 25}}
                        >
                            <Select
                                style={{width: "100%"}}
                                placeholder="Pilih Mouse"
                                onChange={(e)=> {
                                    const filter = DataMouse.filter(it => it.name === e);
                                    setMouse({
                                        name : e,
                                        price: filter[0].price
                                    });
                                }}
                            >
                                {DataMouse?.map((d,idx) => {
                                    return (
                                        <Option key={idx} value={d.name}>
                                            {d.name}
                                        </Option>
                                    );
                                })}
                            </Select>
                        </Form.Item>

                        <Form.Item
                            name="keyboard"
                            label="Keyboard"
                            style={{borderBottom: 25}}
                        >
                            <Select
                                style={{width: "100%"}}
                                placeholder="Pilih Keyboard"
                                onChange={(e)=> {
                                    const filter = DataKeyboard.filter(it => it.name === e);
                                    setKeyboard({
                                        name : e,
                                        price: filter[0].price
                                    });
                                }}
                            >
                                {DataKeyboard?.map((d,idx) => {
                                    return (
                                        <Option key={idx} value={d.name}>
                                            {d.name}
                                        </Option>
                                    );
                                })}
                            </Select>
                        </Form.Item>

                        <Form.Item
                            name="monitor"
                            label="Monitor"
                            style={{borderBottom: 25}}
                        >
                            <Select
                                style={{width: "100%"}}
                                placeholder="Pilih Monitor"
                                onChange={(e)=> {
                                    const filter = DataMonitor.filter(it => it.name === e);
                                    setMonitor({
                                        name : e,
                                        price: filter[0].price
                                    });
                                }}
                            >
                                {DataMonitor?.map((d,idx) => {
                                    return (
                                        <Option key={idx} value={d.name}>
                                            {d.name}
                                        </Option>
                                    );
                                })}
                            </Select>
                        </Form.Item>
                        <Button type="primary"
                                block
                                htmlType="submit"
                                size={'large'}
                                onClick={async ()=> {
                                    try {
                                        const values  = await form.validateFields();
                                        const data = {
                                            processor: values.processor,
                                            motherboard: values.motherboard,
                                            memory:values.memory,
                                            casing:values.casing,
                                            ssd:values.ssd,
                                            vga:values.vga,
                                            mouse:values.mouse,
                                            keyboard:values.keyboard,
                                            monitor:values.monitor,
                                        }
                                        console.log('data search', data);

                                        store.search.searchRecomendation(data);

                                        router.push('/dashboard/search');
                                    } catch (e) {
                                        console.log('gagal search', e)
                                    }

                                }}
                                className="login-form-button">
                            Simpan
                        </Button>
                    </Card>
                </Form>
            </div>

            <div style={{width:'20%'}}>
                {totalPrice !== 0 && (
                    <Card title={"Summary"} style={{position:'fixed', borderTopColor:'red', marginLeft:25}}>
                        {processor.name !== '' && (<p>Processor : {processor.name} (Rp.{processor.price})</p>)}

                        {motherBoard.name !== '' && (<p>MotherBoard : {motherBoard.name} (Rp.{motherBoard.price})</p>)}

                        {memory.name !== '' && (<p>Memory : {memory.name} (Rp.{memory.price})</p>)}

                        {casing.name !== '' && (<p>Casing : {casing.name} (Rp.{casing.price})</p>)}

                        {ssd.name !== '' && (<p>SSD : {ssd.name} (Rp.{ssd.price})</p>)}

                        {vga.name !== '' && (<p>VGA : {vga.name} (Rp.{vga.price})</p>)}

                        {mouse.name !== '' && (<p>Mouse : {mouse.name} (Rp.{mouse.price})</p>)}

                        {keyboard.name !== '' && (<p>Keyboard : {keyboard.name} (Rp.{keyboard.price})</p>)}

                        {monitor.name !== '' && (<p>Monitor : {monitor.name} (Rp.{monitor.price})</p>)}

                        <div>
                            Grand Total : Rp. {totalPrice}
                        </div>
                    </Card>
                )}
            </div>
        </div>
    </div>
});

Dashboard.getLayout = function Layout(page) {
    return <DefaultLayout>
        {page}
    </DefaultLayout>;
};

export default Dashboard;
