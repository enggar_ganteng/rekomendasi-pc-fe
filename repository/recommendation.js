import { http } from "../utils/http";
import useSWR from "swr";
const url = {
    saved: () => "api/getPC",
    getPC: () => "api/getPC",
};

const hooks = {
    getPC() {
        // eslint-disable-next-line react-hooks/rules-of-hooks
        return useSWR(url.getPC(), http.fetcher);
    },
};

const manipulateData = {
    saved(data) {
        return http.post(url.saved()).send(data);
    },
};

const RecommendationRepository = {
    url,
    manipulateData,
    hooks,
};

export default RecommendationRepository;
