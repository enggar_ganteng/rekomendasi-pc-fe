import { http } from "../utils/http";
import useSWR from "swr";

const url = {
  users: () => "/users",
  idUser: (id) => `/users/${id}`,
  detailUser: () => `/users/detail/user`,
  changePassword: () => `/users/reset/change-password`,
};

const hooks = {
  getUsers() {
    return useSWR(url.users(), http.fetcher);
  },
  getDetailUsers(id) {
    return useSWR(url.idUser(id), http.fetcher);
  },
  getDetail() {
    return useSWR(url.detailUser(), http.fetcher);
  },
};

const manipulateData = {
  deleteUser(id) {
    return http.del(url.idUser(id));
  },
  createUser(data) {
    return http.post(url.users()).send(data);
  },
  updateUser(data, id) {
    return http.put(url.idUser(id)).send(data);
  },
  changePassword(data) {
    return http.put(url.changePassword()).send(data);
  },
};

const UserRepository = {
  url,
  manipulateData,
  hooks,
};

export default UserRepository;
