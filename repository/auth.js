import { http } from "../utils/http";
import { httpPasarjaya } from "../utils/http_pasarjaya";

const url = {
  login: () => "api/login",
};

const manipulateData = {
  login(data) {
    return http.post(url.login()).send(data);
  },
};

const AuthRepository = {
  url,
  manipulateData,
};

export default AuthRepository;
